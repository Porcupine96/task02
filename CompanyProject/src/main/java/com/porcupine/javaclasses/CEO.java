package com.porcupine.javaclasses;

public class CEO extends Manager {


    public CEO(String name, int salary) {
        super(name, salary);
    }

    // I assume that the CEO has an unlimited resources when it comes to hiring new employees
    public boolean canHireGivenEmployee(Employee employee) {
        return true;
    }

    public String hireAnEmployee(Employee employee) {

        employeeList.add(employee);
        return "Welcome on board " + employee.getName() + "!\n";
    }
}
