package com.porcupine.javaclasses;

public class Company {

    private Company() {}
    private CEO ceo;

    private static Company company;

    public static Company getInstance() {

        if (company == null) {
            company = new Company();
        }
        return company;
    }

    public void hireCEO(CEO ceo) {
        this.ceo = ceo;
    }

    @Override
    public String toString() {

        if (ceo == null) return "No one has been hired!\n";
        String companyStructure = ceo.getName() + " - CEO\n";

        StringBuilder result = new StringBuilder(companyStructure);

        for (Employee e : ceo.employeeList) {

            if (e.getClass() == BudgetOrientedManager.class || e.getClass() == CounterOrientedManager.class) {

                result.append("\t" + e.getName() + " - Manager \n");

                for (Employee f : ((Manager)e).employeeList) {
                    result.append("\t\t" + f.getName() + " - Employee \n");
                }
            }
            else {
                result.append("\t" + e.getName() + " - Employee \n");
            }
        }
        companyStructure = result.toString();
        return companyStructure;
    }
}
