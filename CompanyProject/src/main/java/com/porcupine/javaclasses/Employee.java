package com.porcupine.javaclasses;

public class Employee {

    private final String name;
    private int salary;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    // we could change the salary later on
    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public String salaryAnswer() {
        return "I earn " + salary + "!\n";
    }

    public String getSatisfaction() {

        if (salary > 10000) {
            return "Yes, I'm quite satisfied!\n";
        }
        else {
            return  "Uhh, It could be better...\n";
        }
    }
}
