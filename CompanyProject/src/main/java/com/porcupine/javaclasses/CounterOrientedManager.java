package com.porcupine.javaclasses;

public class CounterOrientedManager extends Manager {

    private int counter;

    public CounterOrientedManager(String name, int salary, int counter) {
        super(name, salary);
        this.counter = counter;
    }

    public boolean canHireGivenEmployee(Employee employee) {
        return counter > 0;
    }

    public String hireAnEmployee(Employee employee) {

        if (canHireGivenEmployee(employee)) {
            counter--;
            employeeList.add(employee);
            return "Welcome on board " + employee.getName() + "!\n";
        }
        return "Sorry, but I can't hire you.\n";
    }
}
