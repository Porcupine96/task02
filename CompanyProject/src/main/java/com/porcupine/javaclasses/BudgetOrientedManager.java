package com.porcupine.javaclasses;

public class BudgetOrientedManager extends Manager{

    private int budget;

    public BudgetOrientedManager(String name, int salary, int budget) {

        super(name, salary);
        this.budget = budget;
    }

    public boolean canHireGivenEmployee(Employee employee) {
        return employee.getSalary() <= budget;
    }

    public String hireAnEmployee(Employee employee) {

        if (canHireGivenEmployee(employee)) {
            budget -= employee.getSalary();
            employeeList.add(employee);
            return "Welcome on board " + employee.getName() + "!\n";
        }
        return "Sorry, but I can't hire you.\n";
    }
}
