package com.porcupine.javaclasses;

import java.util.ArrayList;
import java.util.List;

abstract public class Manager extends Employee {

    protected List<Employee> employeeList;

    public Manager(String name, int salary) {
        super(name, salary);
        this.employeeList = new ArrayList<Employee>();
    }

    abstract public boolean canHireGivenEmployee(Employee employee);
    abstract public String hireAnEmployee(Employee employee);

        public String canHireGivenEmployeeAnswer(Employee employee) {

        if (canHireGivenEmployee(employee))
            return "Yes, I can hire " + employee.getName() + ".\n";
        else
            return "No, I can't hire " + employee.getName() + ".\n";
    }
}
