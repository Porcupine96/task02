package com.porcupine.javaclasses;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompanyTest {

    Company company;
    CEO zbyszek;

    @Before
    public void setUp() {

        company = Company.getInstance();
        zbyszek = new CEO("Zbyszek", 2000);
        company.hireCEO(zbyszek);
    }

    @Test
    public void shouldReturnCompanyStructure() {

        BudgetOrientedManager maciek = new BudgetOrientedManager("Maciek", 2000, 20000);
        maciek.hireAnEmployee(new Employee("Wacek", 11000));
        maciek.hireAnEmployee(new Employee("Leszek", 3000));
        zbyszek.hireAnEmployee(maciek);
        zbyszek.hireAnEmployee(new Employee("Grzesiek", 1200));
        CounterOrientedManager krzysiek = new CounterOrientedManager("Krzysiek", 2100, 2);
        krzysiek.hireAnEmployee(new Employee("Frodo", 13000));
        zbyszek.hireAnEmployee(krzysiek);

        assertEquals("Zbyszek - CEO\n\tMaciek - Manager \n\t\tWacek - Employee \n\t\tLeszek - Employee \n\tGrzesiek - Employee \n\tKrzysiek - Manager \n\t\tFrodo - Employee \n",
                         company.toString());
    }

}
